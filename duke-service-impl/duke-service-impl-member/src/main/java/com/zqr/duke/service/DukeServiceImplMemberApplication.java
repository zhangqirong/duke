package com.zqr.duke.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DukeServiceImplMemberApplication {

	public static void main(String[] args) {
		SpringApplication.run(DukeServiceImplMemberApplication.class, args);
	}

}
