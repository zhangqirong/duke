package com.zqr.duke.service;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;

@Api
public interface WeiXinAppService {

    @GetMapping("weixinTest")
    public String weixinTest();

}
