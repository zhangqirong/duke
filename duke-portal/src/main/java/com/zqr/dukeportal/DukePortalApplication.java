package com.zqr.dukeportal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DukePortalApplication {

	//dev-101
	//dev-104-1
    //test-dev-105

	public static void main(String[] args) {
		SpringApplication.run(DukePortalApplication.class, args);
	}

}
